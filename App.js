import React from 'react';
import {createAppContainer} from "react-navigation";

import { isSignedIn } from "./src/services/auth";

import { createRootNavigator } from './routes';

const App = createAppContainer(createRootNavigator(!isSignedIn()))

export default App
