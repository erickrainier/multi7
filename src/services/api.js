import React from 'react';
import axios from 'axios';
import { AsyncStorage } from 'react-native';

const api = axios.create({
    baseURL: 'http://multi7qaapi.prolins.com.br/api',     
  });
  export const usuarioToken = token => {
    api.interceptors.request.use(function(config) {
      config.headers.Authorization = `Bearer ${token}`;
      return config;
    });
  };
  export default api;