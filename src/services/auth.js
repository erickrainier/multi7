
import { AsyncStorage } from 'react-native';


export const onSignIn = (token) => AsyncStorage.setItem('TOKEN_KEY', token); //Token de acesso

export const onIdIn =(usuarioid)=>AsyncStorage.setItem('USUARIO_IDKEY',usuarioid); // Async do usuário.

export const onTipoIn = (tipoUser) => AsyncStorage.setItem('TIPOUSER_KEY',tipoUser);



export const onSignOut = () => AsyncStorage.removeItem('TOKEN_KEY');

export const onIdOut =()=>AsyncStorage.removeItem('USUARIO_IDKEY');

export const onTipoOut = () => AsyncStorage.setItem('TIPOUSER_KEY');


export const isSignedIn = async () => {
    const token = await AsyncStorage.getItem('TOKEN_KEY');
};

export const isIdIn = async () => {
    const usuarioid = await AsyncStorage.getItem('USUARIO_IDKEY')
};

export const isTipoIn = async () => {
    const tipoUser = await AsyncStorage.getItem('TIPOUSER_KEY')
};
