import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { MaterialIcons } from '@expo/vector-icons';

export default function TopBlueHeader() {
    return (
        <View style={StyleSheet.header}>
        {/*Icon for menu*/}
            <View>
             <Text style={StyleSheet.headerText}>Perfil</Text>
            </View>
        
        </View>
    );
}

const styles = StyleSheet.create ({
    header:{
        width:'80%',
        height: '80%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    headerText: {
        fontWeight: 'bold',
        fontSize: 20,
        color: '#03A9F4',
        letterSpacing: 1,
    }
})