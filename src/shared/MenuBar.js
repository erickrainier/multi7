
import React,{ Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Container, Header, Button, 
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Drawer, Content, } from 'react-native-gesture-handler/DrawerLayout';


class MenuBar extends Component {
    
    
    render(){
        
        return (
                <View style={[ styles.container, { backgroundColor: '#fff' } ]}>
                        <Text>
                            <Icon name="rocket" size={30} color="#900" />
                            
                        </Text>
                </View>
               );
    } 
};

export default class MenuBarDrawer extends Component () {
  closeDrawer = () => {
      this.drawer._root.close()
  };
  openDrawer = () => {
      this.drawer._root.open()
  };    
  render() {
    return (
        <Drawer
        ref={(ref) => { this.drawer = ref; }}
        content={<MenuBar navigator={this.navigator} />}
        onClose={() => this.closeDrawer()}>
        <Container>
        <Header>
            <Container style={{flexDirection: 'row'}}>
                    <Icon onPress={() => this.openDrawer()} name="bars" size={30} color="#fff" />
            </Container>
        </Header>
          
          <View style={styles.container}>
            <Text style={styles.welcome}>
              Welcome to React Native!
            </Text>
            <Text style={styles.instructions}>
              To get started, edit App.js
            </Text>
            <Text style={styles.instructions}>
              {instructions}
            </Text>
          </View>
         
        </Container>
      </Drawer>
      
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
