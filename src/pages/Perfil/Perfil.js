import React, {useEffect} from "react";
import { View } from "react-native";
import { Card, Button, Text } from "react-native-elements";
import {isSignedIn, onSignOut} from "../../services/auth";
import { TitulosinRoutes } from '../../../routes';
import Header from "../../shared/Header";

export default ({ navigation }) => {

    /* Constante para o tolken*/
    
        useEffect(()=>{
            if(!isSignedIn()){
                navigation.navigate("Sair")
            }
        },[])
    
    /* Axios with tolken(WORKING-ON)*/
    
        var config = {
            headers: {'auth': 'isSignedIn'}
          };
          
          axios.get('/api/tipo-titulo', config);
          axios.post('/titulos', { firstName: 'Erick' }, config);
    
    /*Corpo da aplicação*/
    
    return  ( 
        <View style={{ paddingVertical: 20 }}>

            <View
                style={{
                    backgroundColor: "#bcbec1",
                    alignItems: "center",
                    justifyContent: "center",
                    width: 80,
                    height: 80,
                    borderRadius: 40,
                    alignSelf: "center",
                    marginBottom: 20
                }}
            >
                 
                <Text style={{ color: "white", fontSize: 28 }}>M7</Text>
                
                

            </View>

            <Card title="Titulos">
            <Text style={{ color: "#bcbec1", fontSize: 15,justifyContent:'flex-start' }}>Nome:</Text>
            <Text style={{ color: "#bcbec1", fontSize: 15 }}>Email:</Text>
            <Button
                backgroundColor="#03A9F4"
                title="Editar"
                onPress={() => onTitulos().then(() => navigation.navigate("Editar"))}
            />

            </Card>


            <TouchableOpacity 
            style={styles.detailsButton}                
            onPress = {() => {}}>
                <Text style={ styles.detailsButtonText}>
                Editar
                </Text>
            </TouchableOpacity>


             <Button
                backgroundColor="#03A9F4"
                title="Sair"
                onPress={() => onSignOut().then(() => navigation.navigate("Sair"))}
            />

        </View>
    )
}