import { StatusBar } from 'expo-status-bar'; 
import React, {useState, useEffect} from 'react';
import { 
  StyleSheet, 
  Text, 
  View, 
  ImageBackground,
  Dimensions, 
  TextInput, 
  TouchableOpacity,
  KeyboardAvoidingView,
  Animated,
  AsyncStorage
} from 'react-native';

import { SignedOutRoutes, SignedInRoutes } from './routes';
import api from '../../services/api'

export default function Auth() {

  state = {
    errorMessage: null,
  }

  signIn = async () => {
    try{
      const response = await api.post('/auth/login',{
        cnpjCpf: '',
        email:'',
        senha:'',
      });
  
      const {tokenAcesso, id, email} = response.data;
      
      await AsyncStorage.multiSet([
        ['@CodeApi:token', token],
        ['@CodeApi:user', JSON.stringify(user)]
      ]);

    } catch (response){
      this.setState({errorMessage:response.data.error})
    }


  };

// Animação de subida das caixas
  const [offset] = useState(new Animated.ValueXY({x: 0, y: 80}))
  const [opacity] = useState(new Animated.Value(0));


  useEffect(()=> {
    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 4,
        bounciness: 10
    }),
    Animated.timing(opacity, {
      toValue: 1,
      duration: 200,
    })
    
  ]).start()
  }
  )
  return (
    
      
    <View style={styles.logo}>
      <Image source={require('./src/img/logo.png')}></Image>
    </View>,

    <Animated.View 
    style={[
      styles.container,
      {
        opacity: opacity,
        transform:[
          {translateY: offset.y}
        ]
      }
    ]}>
      <TextInput
      style={styles.input}
      placeholder='CNPJ/CPF'
      autoCorrect={false}
      onChangeText={()=>{}}
      />

      <TextInput
      style={styles.input}
      placeholder='Email'
      autoCorrect={false}
      onChangeText={()=>{}}
      />

      <TextInput
      style={styles.input} 
      placeholder='Senha'
      autoCorrect={false}
      onChangeText={()=>{}}
      />

  <TouchableOpacity style={styles.btnSubmit}> {
   this.state.errorMessage &&<Text>{this.state.errorMessage}</Text>} 
      onPress={this.signIn}>
        <Text style={styles.submitText}>Acessar</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.btnSenha}>
        <Text style={styles.senhaText}>Esqueci a Senha</Text>
      </TouchableOpacity>
    </Animated.View>
//Caixas de autenticação
    
    
  );
}

//Daqui pra baixo é só o styles - Separar para a pasta.
const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: null,
    height: null,
    backgroundColor: '#d3d3d3',
    alignItems: 'center',
    justifyContent: 'center',
  },

  title: {
    color: '#ffF',
    fontSize: 22,
  },

  input: {
    backgroundColor: '#FFF',
    color: '#d3d3d3',
    marginBottom: 15,
    fontSize: 16,
    width: '90%',
    borderRadius: 8,
    padding: 10,
    
  },
  logo:{
    flex: 1,
    justifyContent:'center',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnSubmit:{
    backgroundColor:'#35aaff',
    width:'90%',
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 7
  },
  submitText: {
    color:'#fff',
    fontSize: 18
  },
  btnSenha: {
    marginTop: 12,
  },
  senhaText: {
    color: '#000000',
  }
})
