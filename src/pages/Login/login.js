import React, { useState, useEffect } from 'react';
import { View, Image, StyleSheet } from "react-native";
import { Card, Button, Input } from "react-native-elements";
import TextInput from "react-native-web/dist/exports/TextInput";
import {
  Text, ImageBackground, Dimensions, TouchableOpacity,
  KeyboardAvoidingView, Animated
} from 'react-native';
import axios from 'axios';
import { api } from '../../services/api'
import auth, { onSignIn, onIdIn, onTipoIn } from '../../services/auth'
import routes from '../../../routes'
import styles from './login_styles'
import Titulos from '../Titulos/Titulos'
export default ({ navigation }) => {

  const [seuCnpj, setSeuCnpj] = useState('');
  const [seuEmail, setSeuEmail] = useState('');
  const [suaSenha, setSuaSenha] = useState('');

  // Animação de subida das caixas
  const [offset] = useState(new Animated.ValueXY({ x: 0, y: 80 }))
  const [opacity] = useState(new Animated.Value(0));


  useEffect(() => {
    Animated.parallel([
      Animated.spring(offset.y, {
        toValue: 0,
        speed: 4,
        bounciness: 10
      }),
      Animated.timing(opacity, {
        toValue: 1,
        duration: 200,
      })

    ]).start()
  }
  )
  return (
    <View>
      <View>
      
      </View>
      
      <View>
        <Card>
          <Text>Multi 7</Text>
        </Card>
      </View>

      <View style={{ paddingVertical: 120 }}>
        <Card cardBorderRadius={8}>
          <Input placeholder="CNPJ" onChange={(e) => setSeuCnpj(e)} />
          <Input placeholder="Email" onChange={(e) => setSeuEmail(e)} />
          <Input secureTextEntry placeholder="Digite sua senha"
            onChange={(e) => setSuaSenha(e)} />
          <Button buttonStyle={{ marginTop: 20 }} backgroundColor="#03A9F4"
            title="Entrar"
            onPress={() => {
              if (seuCnpj && seuEmail && suaSenha) {
                const resp = axios.post('http://multi7qaapi.prolins.com.br/api/auth/login',
                  {
                    cnpjCpf: "20115353000144",
                    email: "naldson.chagas@prolins.com.br",
                    senha: "12345678"
                  })
                  .then(async function (response) {
                    if (response.status == 200) {
                      const token = response.data.tokenAcesso
                      const tipoPessoa = response.data.usuarioToken.claims[6].valor
                      const usuarioId = response.data.usuarioToken.id
                      await onSignIn(token),
                      onTipoIn(tipoPessoa),
                      onIdIn(usuarioId),
                      navigation.navigate('Titulos')
                    } else {
                      //ostrar notificacao
                    }

                    console.log(response) // Console está mostrando o Token 
                  })
              } else {
                alert('Preencha todos os campos')
              }
            }}
          />
          <TouchableOpacity style={styles.btnSenha}>
            <Text style={styles.senhaText}>Esqueci a Senha</Text>
          </TouchableOpacity>
        </Card>
      </View>
    </View>


  )


}