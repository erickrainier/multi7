import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    background: {
      flex: 1,
      width: null,
      height: null,
      backgroundColor: '#d3d3d3',
      alignItems: 'center',
      justifyContent: 'center',
    },
  
    title: {
      color: '#ffF',
      fontSize: 22,
    },
  
    input: {
      backgroundColor: '#FFF',
      color: '#d3d3d3',
      marginBottom: 15,
      fontSize: 16,
      width: '90%',
      borderRadius: 8,
      padding: 10,
      
    },
    logo:{
      flex: 1,
      justifyContent:'center',
    },
    container: {
      alignItems: 'center',
      justifyContent: 'center',
    },
    btnSubmit:{
      backgroundColor:'#35aaff',
      width:'90%',
      height: 45,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 7
    },
    submitText: {
      color:'#fff',
      fontSize: 18
    },
    btnSenha: {
      marginTop: 12,
    },
    senhaText: {
      color: '#000000',
      textAlign: 'right',
    }
  })