import React from 'react';
import { Feather } from '@expo/vector-icons';
import { Icon } from 'react-native-vector-icons'
import { axios } from 'axios';
import { View, Image, TouchableOpacity, createStackNavigator, Text } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Card, Button } from 'react-native-elements';
import styles from './styles'
import Titulos from '../Titulos/Titulos'
import { AsyncStorage } from 'react-native';

function navigateBack() {
    navigation.goBack('Titulos')

}
export default function Detail() {

    const navigation = useNavigation();
    const route = useRoute();

    const lista = route.params.lista;

    const token = await AsyncStorage.getItem('TOKEN_KEY')
    const tipoPessoa = await AsyncStorage.getItem('TIPOUSER_KEY')
    const usuarioId = await AsyncStorage.getItem('USUARIO_IDKEY')
    let lowercasetext = tipoPessoa.toLowerCase()
//Const de requisiçao da api.
const response = await api.get(`http://multi7qaapi.prolins.com.br/api/titulo/obter-boleto-sacado?cnpjCpf=${lowercasetext}&tituloId=${tituloId}/`,
{                               
    headers: {
        Authorization: `Bearer ${token}`

    }
};
    return (
        <View>
            <View >

                <Card style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', flex: 1, }}>

                    <Text style={styles.headerText}>
                        {Intl.NumberFormat('pt-BR', {
                            style: 'currency',
                            currency: 'BRL'
                        }).format(lista.valor)}
                    </Text>

                    <View style={styles.containerEstaticos}>

                        <Text style={styles.billetValue}>Número do Titulo</Text>
                        <Text style={styles.billetValue}>Data da Compra</Text>
                        <Text style={styles.billetValue}>Data de Vencimento</Text>
                        <Text style={styles.billetValue}>Valor Original</Text>
                        <Text style={styles.billetValue}>Valor Desconto</Text>
                        <Text style={styles.billetValue}>Número</Text>

                    </View>

                    <View style={styles.containerValores}>

                        <Text style={styles.IdBillet}>{lista.tituloId}</Text>
                        <Text style={styles.IdBillet}>{lista.dataCompra}</Text>
                        <Text style={styles.IdBillet}>{lista.valorOriginal}</Text>
                        <Text style={styles.IdBillet}>{lista.valorDesconto}</Text>
                        <Text style={styles.IdBillet}>{lista.dataVencimento}</Text>
                        <Text style={styles.IdBillet}>{lista.nossoNumero}</Text>

                    </View>




                </Card>
            </View>
            
            <TouchableOpacity style={styles.Button} >
                <View style={styles.Button} >
                    <Feather name="download" size={25} color="#03A9F4" />
                    <Text style={styles.textButton}>Baixar Boleto</Text>
                </View>
            </TouchableOpacity>



        </View>


    )
}

<Card style={styles.Button}>
    <TouchableOpacity style={styles.Button} >
        <View style={styles.Button} >
            <Feather name="download" size={25} color="#03A9F4" />
            <Text style={styles.textButton}>Baixar Boleto</Text>
        </View>
    </TouchableOpacity>
</Card>
