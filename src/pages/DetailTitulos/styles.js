import { StyleSheet } from 'react-native';
import Constants from 'expo-constants';

export default StyleSheet.create({
    container:{
        flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start' // if you want to fill rows left to right
    },

    header:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    headerText: {
        fontSize: 20,
        color: '#35aaff',
        fontWeight: 'bold',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    button: {
        borderRadius: 8,
        paddingVertical: 14,
        paddingHorizontal: 10,
        backgroundColor: '#35aaff'
    },
    textButton: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center', 
        fontSize: '18',
        backgroundColor: '#35aaff'
    },
    title:{
        fontSize: 30,
        marginBottom: 16,
        marginTop: 32 
    },
    IdBillet:{
        fontSize: 15,
        color: '#35aaff',
    },
    TitulosList: {
        marginTop: 32
    },
    billetValue:{
        fontSize: 15,
        fontWeight: 'bold',
    },
    detailsButton:{
        fontSize: 15,
        color: '#35aaff',
    },
    containerEstaticos: {
        flex: 1/2,
        flexDirection: 'column',
        flexWrap: 'nowrap ',
        textAlign: 'left',
        justifycontent: 'left'

      },
    containerValores: {
        flex: 2/2,
        flexDirection: 'column',
        flexWrap: 'nowrap ',
        textAlign: 'right', // if you want to fill rows left to right
        justifycontent: 'right',
        textAlignVertical: 'right'
      },
})
