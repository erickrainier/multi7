import { StyleSheet } from 'react-native';
import Constants from 'expo-constants';

export default StyleSheet.create({
    container:{
        flex: 1,
        paddingHorizontal: 24,
        paddingTop: Constants.statusBarHeight + 20,
    },

    header:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    headerText: {
        fontSize: 15,
        color: '#7332',
    },
    headerTextBold: {
        fontWeight: 'bold'
    },
    title:{
        fontSize: 30,
        marginBottom: 16,
        marginTop: 32 
    },
    IdBillet:{
        fontSize: 15,
        color: '#35aaff',
    },
    TitulosList: {
        marginTop: 32
    },
    billetValue:{
        fontSize: 15,
        color: '#d3d3d3',
    }
})

