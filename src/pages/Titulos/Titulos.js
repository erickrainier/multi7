import React from 'react';
import { Text, View } from 'react-native';
import { useEffect, useState } from "react"; // useEffect => serve para carregar comp em tela 
import { createStackNavigator, TouchableOpacity } from 'react-native';
import { Card, Button } from "react-native-elements";
import { Feather } from '@expo/vector-icons';
import { axios } from 'axios';
import api from '../../services/api'
import { onIdIn } from '../../services/auth'
import { AsyncStorage } from 'react-native';
import Detail from '../DetailTitulos/Detail'
import styles from './styles'
import { FlatList } from 'react-native-gesture-handler';
import { useNavigation, useRoute } from '@react-navigation/native';




export default ({ navigation }) => {

    const [PagTitulos, setPagTitulos] = useState(false);
    const [total, setTotal] = useState(0);
    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(false);


    function navigateToDetail(lista) {
        navigation.navigate('Detail', { lista });

    }

    async function loadTitulos() {


        const token = await AsyncStorage.getItem('TOKEN_KEY')
        const tipoPessoa = await AsyncStorage.getItem('TIPOUSER_KEY')
        const usuarioId = await AsyncStorage.getItem('USUARIO_IDKEY')
        let lowercasetext = tipoPessoa.toLowerCase()


        if (loading) {
            return;
        }

        if (total > 0 && PagTitulos.length == total) {
            return;
        }
        setLoading(true);

        //Const de requisiçao da api.
        const response = await api.get(`http://multi7qaapi.prolins.com.br/api/titulo/paginado/${lowercasetext}/?filtro=${usuarioId}/`,
            {
                headers: {
                    Authorization: `Bearer ${token}`

                },
                params: { page },
                timeout: 5000
            }).catch(error => {
                console.log(error)
            })
            ;

        try{
            setPagTitulos([...response.data.lista]).
            setTotal(response.headers['']);
            setPage(page + 1);
            setLoading(false);
        }catch(error) {alert('Falha no servidor. Reinicie o App')};
        
    }


    useEffect(() => {
        loadTitulos({});
    }, [PagTitulos]);

    let formatter = new Intl.DateTimeFormat('pt-BR')

    return (
        <View>
            <FlatList
                data={PagTitulos} //data das informações a serem preenchidas.
                style={styles.TitulosList}
                keyExtractor={(item) => String(item.lista)} //keyExtractor joga todas as info em comum de cada item
                showsVerticalScrollIndicator={false} //tira a barra de rolagem da lista
                onEndReached={loadTitulos}
                onEndReachedThreshold={0.2}
                renderItem={({ item: lista }) => (
                    < View style={{ marginBottom: 10, marginLeft: 5, marginRight: 5, borderRadius: 2, padding: 10 }}>
                        <Card>
                            <TouchableOpacity style={styles.detailsButton}
                                onPress={() => navigateToDetail(lista)
                                }>
                                <Feather name="file-text" size={22} color="#03A9F4" />
                                <Text style={styles.IdBillet}>
                                    <Text style={styles.billetValue}>{lista.dataVencimento}</Text>
                                    <Text style={styles.IdBillet}>{lista.numeroDocumento}</Text>
                                    <Text style={styles.IdBillet}>
                                        {Intl.NumberFormat('pt-BR', {
                                            style: 'currency',
                                            currency: 'BRL'
                                        }).format(lista.valor)}
                                    </Text>
                                </Text>
                            </TouchableOpacity>
                        </Card>
                    </View >

                )}
            />
            <View>
                <Button buttonStyle={{ marginTop: 20 }} backgroundColor="#03A9F4"
                    title="Clique para carregar mais"

                />
            </View>
        </View>

    )

}



