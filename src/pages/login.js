import React, {useState, useEffect} from 'react';
import { View,  } from "react-native";
import { Card, Button,  Input} from "react-native-elements";
import { onSignIn } from "../services/auth";
import TextInput from "react-native-web/dist/exports/TextInput";
import { 
    StyleSheet, 
    Text, 
    ImageBackground,
    Dimensions, 
    TouchableOpacity,
    KeyboardAvoidingView,
    Animated
  } from 'react-native';
  

export default ({ navigation }) => {

    const  [textInput1, setTextInput1] = useState('')
    const  [textInput2, setTextInput2] = useState('')
    const  [textInput3, setTextInput3] = useState('')

    

return <View style={{ paddingVertical: 20 }}>

    <Card>
        

        <Input 
        placeholder="CNPJ" 
        onChange={(e)=> setTextInput1(e)}  />
   
        <Input 
        placeholder="Email" 
        onChange={(e)=> setTextInput2(e)}  />

        <Input 
        secureTextEntry 
        placeholder="Digite sua senha" 
        onChange={(e)=> setTextInput3(e)}  />

        <Button
            buttonStyle={{ marginTop: 20 }}
            backgroundColor="#03A9F4"
            title="Entrar"
            onPress={() => {


                if(textInput1 && textInput2){
                    onSignIn().then(() => navigation.navigate("SignedIn"));
                } else {
                    alert('Preencha os campos')
                }



            }}
        />
    </Card>
</View>
}
