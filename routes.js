import { NavigationContainer } from '@react-navigation/native'
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Login from './src/pages/Login/login';
import Logged from './src/pages/logged';

import TopBlueHeader from './src/shared/Header';

import Titulos from './src/pages/Titulos/Titulos'
import Cedentes from './src/pages/Cedentes/Cedentes_index'
import Perfil from './src/pages/Perfil/Perfil';
import Detail from './src/pages/DetailTitulos/Detail'

import isSignedIn from './src/services/auth'

const Stack = createStackNavigator()
const Drawer = createDrawerNavigator()


const DrawerScreens = () => <Drawer.Navigator initialRouteName="Titulos">       
    <Drawer.Screen name="Titulos" component={Titulos} />
    <Drawer.Screen name="Cedentes" component={Cedentes} />
    <Drawer.Screen name="Perfil" component={Perfil} />
</Drawer.Navigator>

const App = () => {
    return <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen
                name='Login'
                component={Login}                
            />
            
            <Stack.Screen
                name='Titulos'
                component={Titulos}
                headers={isSignedIn}
            />
            <Stack.Screen
                name='Header'
                component={TopBlueHeader}
                headers={isSignedIn}
            />
            <Stack.Screen
                name='Detail'
                component={Detail}
                headers={isSignedIn}
            />
            <Stack.Screen
                name='DrawerScreens'
                component={DrawerScreens}
                headers={isSignedIn}
            />
        </Stack.Navigator>
    </NavigationContainer>
}
export default App